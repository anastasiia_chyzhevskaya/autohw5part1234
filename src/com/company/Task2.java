package com.company;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Scanner;

public class Task2 {
    /*2) Второе задание

    Для выполнения задания понадобиться Scanner s = new Scanner(System.in);

    Необходимо ввести 10 значений с клавиатуры  и сохранить их в array, после этого вывести на экран.
    */

    public static void task2() {
        int[] ints = new int[10];
        Scanner s = new Scanner(System.in);
        System.out.println("Вам необходимо ввести 10 чисел");
        for (int i = 0; i < 10; i++) {
            System.out.println("число " + (i + 1));
            ints[i] = s.nextInt();
            if (i == 9) {
                for (int j = 0; j < 10; j++) {
                    System.out.print(ints[j] + " ");
                }
            }
        }
    }
}
