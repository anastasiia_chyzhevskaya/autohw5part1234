package com.company;

public class Task3 {
    public static void task3() {
        /*3) Третье задание

        Найти самое маленькое и самое большое значение в array и вывести их на экран. Возьмите размер array = 8.
        */
        int minValue = Integer.MAX_VALUE;
        int maxValue = Integer.MIN_VALUE;

        int[] ints = {6, 76, 33, 111, 34, 566, -45, 123};

        for (int i = 0; i < ints.length; i++) {
            if (ints[i] >= maxValue) {
                maxValue = ints[i];
            }
            if (ints[i] <= minValue) {
                minValue = ints[i];
            }
        }

        System.out.println("Самое большое значение в массиве равно: " + maxValue);
        System.out.println("Самое маленькое значение в массиве равно: " + minValue);
    }
}
