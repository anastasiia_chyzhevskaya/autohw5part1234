package com.company;

public class Task1 {
    /*1) Первое задание
    1. Необходимо создать 2 массива из 10 случайных целых чисел в диапазоне от [0;10] каждый
    2. Необходимо вычислить  среднее арифметическое элементов каждого массива, если для первого
    массива среднеарифметическое значение оказалось больше, то выведите это через (sout),
    либо вывести что их среднеарифметические значения равны.
    */

    public static void task1() {
        int[] first = {1, 3, 5, 6, 4, 2, 9, 8, 1, 2};
        int[] second = {8, 4, 7, 5, 3, 1, 1, 1, 1, 1};

        double mediumOne = returnMediumAriphmetic(first);
        double mediumTwo = returnMediumAriphmetic(second);

        if (mediumOne > mediumTwo) {
            System.out.println("Среднее арифметическое первого массива чисел больше чем второго массива и равно: " + mediumOne);
        } else if (mediumOne < mediumTwo) {
            System.out.println("Среднее арифметическое второго массива чисел больше чем первого массива и равно: " + mediumTwo);
        } else {
            System.out.println("Средние арифметические первого массива и второго массива равны: " + mediumOne + " и " + mediumTwo);
        }
    }

    public static double returnMediumAriphmetic(int[] ints) {
        double value = 0;
        double sum = 0;
        for (int i = 0; i < ints.length; i++) {
            sum += ints[i];
            if (i == ints.length - 1) {
                value = sum / ints.length;
            }
        }
        return value;
    }
}
